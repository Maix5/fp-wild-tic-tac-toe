{-# LANGUAGE InstanceSigs #-}
module ActionsMove where

import Debug.Trace

import Data.List
import Data.Maybe
import Data.Bifunctor

import Parse.Bencode
import Parse.TicTacToeMsg
import Parse.MsgPlusMap
import Utils hiding (fromJust)

import GameInfo
import qualified ActionsWinner as Win

import qualified Data.Tree.Game_tree.Game_tree as GT
import qualified Data.Tree.Game_tree.Negascout as NG



centerCoordinate  = Coordinate 1 1
inicialCoordinate = Coordinate 0 0
inicialValue      = X

searchDepth :: Integer -> Integer
searchDepth moveNum
  | moveNum >= 0 && moveNum <= 2 = 3
  | otherwise                    = boardSize - moveNum


class MinimaxData a where
    isTerminalState :: a -> Bool
    heuristicValue :: a -> Int
    childrens :: a -> [a]


instance MinimaxData GameInfo where
    isTerminalState :: GameInfo -> Bool
    isTerminalState = Win.winnerCheck

    heuristicValue :: GameInfo -> Int
    heuristicValue gInfo
      -- | trace ("gameInfo xBoard - " ++ show xB) False = undefined
      -- | trace ("gameInfo oBoard - " ++ show oB) False = undefined

      | isTerminalState gInfo && isMyTurn gInfo      = 10 - num
      -- | isTerminalState gInfo && isOponentTurn gInfo = -1
      | otherwise = (-10) + num
      where
          -- xB = xBoard gInfo
          -- oB = oBoard gInfo

          num = fromIntegral $ moveNum gInfo


    childrens :: GameInfo -> [GameInfo]
    childrens gInfo
      | inicialCoordinate `elem` xB = oGameInfos ++ xGameInfos
      | otherwise                   = xGameInfos ++ oGameInfos
        where
            xB = xBoard gInfo
            oB = oBoard gInfo

            newMoveGInfo     = gInfo { moveNum = (+1) $ moveNum gInfo }
            newXBoard xCoord = newMoveGInfo { xBoard = xCoord : xB }
            newOBoard oCoord = newMoveGInfo { oBoard = oCoord : oB }

            xGameInfos = map newXBoard $ getOnlyNewMoves (xB ++ oB)
            oGameInfos = map newOBoard $ getOnlyNewMoves (xB ++ oB)


--- Custom MINIMAX {{{1

-- minimax :: (Ord f, Fractional f)
--         => GameInfo -> Integer -> Bool -> f
-- -- State, Depth, MaxizingPlayer
-- minimax state depth maximizingPlayer
--   | trace ("maximizingPlayer - " ++ show maximizingPlayer) False = undefined
--   | trace ("gameInfo xBoard - " ++ show xB) False = undefined
--   | trace ("gameInfo oBoard - " ++ show oB) False = undefined
--   | trace ("gameInfo eval - " ++ show gv) False = undefined
--   | trace ("gameInfo leaf - " ++ show leaf) False = undefined
--
--   | depth == 0 || isTerminalState state = heuristicValue state
--   | maximizingPlayer = foldr (\ s v -> max v $ minimax s newD False) (-inf) cs
-- -- minimizing player
--   | otherwise        = foldr (\ s v -> min v $ minimax s newD True) inf cs
--     where
--         xB = xBoard state
--         oB = oBoard state
--         gv = heuristicValue state
--         leaf = depth == 0 || isTerminalState state
--
--
--         inf    = 1/0
--         cs = childrens state
--         newD = depth - 1


-- selectMove' :: GameInfo -> (Float, GameInfo) -> (Float, GameInfo)
-- selectMove' gInfo (bestScore

-- selectMove :: GameInfo -> GameInfo
-- selectMove

-- testMinimax state depth = minimax state depth True


--- }}}1


---- Lib MINIMAX {{{1


instance GT.Game_tree GameInfo where
    is_terminal :: GameInfo -> Bool
    is_terminal = isTerminalState

    node_value :: GameInfo -> Int
    node_value = heuristicValue

    children :: GameInfo -> [GameInfo]
    children = childrens


selectGameInfo :: GameInfo -> Maybe GameInfo
selectGameInfo gInfo
  | moveNum gInfo == 0 = Just gInfo { xBoard = [inicialCoordinate] }
  | otherwise = finalMoves !!? 1
    where
        mov = moveNum gInfo
        depth = fromIntegral $ searchDepth mov
        (finalMoves, score) = NG.alpha_beta_search gInfo depth


selectMove :: GameInfo -> Maybe (TicTacToeMsgValue, TicTacToeMsgCoordinate)
selectMove gInfo
  -- | trace ("gameInfo num - " ++ show mov) False = undefined
  -- | trace ("gameInfo xBoard - " ++ show xB) False = undefined
  -- | trace ("gameInfo oBoard - " ++ show oB) False = undefined
  -- | trace ("gameInfo New xBoard - " ++ show newXB) False = undefined
  -- | trace ("gameInfo New oBoard - " ++ show newOB) False = undefined
  -- | trace ("gameInfo diff xBoard - " ++ show diffXB) False = undefined
  -- | trace ("gameInfo diff oBoard - " ++ show diffOB) False = undefined
  | moveNum gInfo == 0 = Just (inicialValue, inicialCoordinate)
  | not $ null diffXB  = (\x -> return (X, x)) =<< diffXB
  | not $ null diffOB  = (\x -> return (O, x)) =<< diffOB
  | otherwise          = Nothing
    where
        mov = moveNum gInfo
        depth = fromIntegral $ searchDepth mov
        (finalMoves, score) = NG.alpha_beta_search gInfo depth
        finalMove = finalMoves !!? 1

        xB = xBoard gInfo
        oB = oBoard gInfo

        newXB = xBoard <$> finalMove
        newOB = oBoard <$> finalMove

        diffXB = (!!? 0) =<< (xB `listsUnionIntersDiff`) <$> newXB
        diffOB = (!!? 0) =<< (oB `listsUnionIntersDiff`) <$> newOB



moveBasic :: Bool
          -> [TicTacToeMsg]
          -> Either String (TicTacToeMsgValue, TicTacToeMsgCoordinate)
moveBasic iStartedGame interp = do
    gi <- toGameInfo iStartedGame interp
    case selectMove gi of
      Just d -> return d
      Nothing -> Left errMsg
    where errMsg = "Could not select next move."

move :: String -> Either String (Maybe (Int, Int, Char))
{-|
    Calculates if specified "Wild Tic-Tac-Toe" game encoded in
    **Bencode** string next possible move.
    Returns either errorMsg or a coordinates and placed sign.
-}
move s = do
    interp <- interpretateMsg $ parseBencode s
    d <- moveBasic True interp
    let (val, coords) = d
        xC = fromIntegral $ coordianteX coords
        yC = fromIntegral $ coordinateY coords
        sign = head $ show val
    return $ Just (xC, yC, sign)





-- }}}1



-- Moves generator {{{1

testBoardMoves = (allMoves `listsUnionIntersDiff` newMoves) == testB
    where
        allMoves = generateAllMoves
        testB = [Coordinate 0 0, Coordinate 0 1, Coordinate 0 2]
        newMoves = getOnlyNewMoves testB

generateAllMoves :: [TicTacToeMsgCoordinate]
generateAllMoves =
    [ Coordinate x y
    | x <- [0..boardChunkSize -1]
    , y <- [0..boardChunkSize -1]
    ]

getOnlyNewMoves :: [TicTacToeMsgCoordinate] -> [TicTacToeMsgCoordinate]
getOnlyNewMoves currentBoard =
    currentBoard `listsUnionIntersDiff` generateAllMoves

-- }}}1


-- Testing data {{{1

testGameInfo1 =
    (emptyGameInfo True)
        { playerName1 = Just $ UserId "1"
        , moveNum = 1
        , xBoard = [Coordinate 1 1]
        }
testGameInfo2 =
    (emptyGameInfo False)
        { playerName1 = Just $ UserId "1"
        , playerName2 = Just $ UserId "2"
        , moveNum = 2
        , xBoard = [Coordinate 0 1, Coordinate 0 0]
        }
testGameInfo3_1 =
    (emptyGameInfo False)
        { playerName1 = Just $ UserId "1"
        , playerName2 = Just $ UserId "2"
        , moveNum = 2
        , xBoard = [Coordinate 1 1, Coordinate 0 0]
        }
testGameInfo3_2 =
    (emptyGameInfo False)
        { playerName1 = Just $ UserId "1"
        , playerName2 = Just $ UserId "2"
        , moveNum = 2
        , xBoard = [Coordinate 0 0]
        , oBoard = [Coordinate 1 1]
        }
testGameInfo4 =
    (emptyGameInfo False)
        { playerName1 = Just $ UserId "1"
        , playerName2 = Just $ UserId "2"
        , moveNum = 2
        , xBoard = [Coordinate 1 1]
        , oBoard = [Coordinate 0 0]
        }
testGameInfo4_1 =
    (emptyGameInfo True)
        { playerName1 = Just $ UserId "1"
          , playerName2 = Just $ UserId "2"
          , moveNum = 5
          , xBoard = [Coordinate 1 1]
          , oBoard = [Coordinate 0 0, Coordinate 0 2, Coordinate 2 0, Coordinate 0 1]
        }
testGameInfo4_2 =
    (emptyGameInfo True)
        { playerName1 = Just $ UserId "1"
          , playerName2 = Just $ UserId "2"
          , moveNum = 3
          , xBoard = [Coordinate 1 1]
          , oBoard = [Coordinate 0 0, Coordinate 2 0]
        }
testGameInfo4_3 =
    (emptyGameInfo True)
        { playerName1 = Just $ UserId "1"
          , playerName2 = Just $ UserId "2"
          , moveNum = 5
        , xBoard = [Coordinate 1 1, Coordinate 2 2]
          , oBoard = [Coordinate 0 0, Coordinate 0 2, Coordinate 2 0]
        }
testGameInfo5 =
    (emptyGameInfo True)
        { playerName1 = Just $ UserId "1"
          , playerName2 = Just $ UserId "2"
          , moveNum = 3
          , oBoard = [Coordinate 0 0, Coordinate 0 1, Coordinate 0 2]
        }

testGameInfo6 =
    (emptyGameInfo True)
        { playerName1 = Just $ UserId "1"
          , playerName2 = Just $ UserId "2"
          , moveNum = 4
        , xBoard = [Coordinate 1 1]
        , oBoard = [Coordinate 2 1, Coordinate 1 2, Coordinate 0 0]
        }

testGameInfo7 =
    (emptyGameInfo False)
        { playerName1 = Just $ UserId "1"
          , playerName2 = Just $ UserId "2"
          , moveNum = 3
          , xBoard = [Coordinate 1 1]
          , oBoard = [Coordinate 1 2, Coordinate 0 0]
        }

testGameInfo8 =
    (emptyGameInfo False)
        { playerName1 = Just $ UserId "1"
          , playerName2 = Just $ UserId "2"
          , moveNum = 7
        , xBoard = [Coordinate 0 2, Coordinate 1 0, Coordinate 2 2, Coordinate 1 1 ]
        , oBoard = [Coordinate 2 0, Coordinate 1 2, Coordinate 0 0]
        }

-- }}}1
