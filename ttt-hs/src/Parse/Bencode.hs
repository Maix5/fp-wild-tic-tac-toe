module Parse.Bencode
    ( BencodeData(..)

    , TypeBencodeInteger
    , TypeBencodeString
    , TypeBencodeList
    , TypeBencodeDictionary, TypeBencodeDictionaryElem

    , parseBencode
    -- , parseInteger, parseString, parseList, parseDictionary

    , composeBencode
    -- , composeInteger, composeString, composeList, composeDictionary

    -- , basicTestIntegerG1, basicTestIntegerG2            -- For testing.
    -- , basicTestIntegerB1                                -- For testing.
    -- , basicTestStringG1, basicTestStringG2              -- For testing.
    -- , basicTestStringB1, basicTestStringB2              -- For testing.
    -- , basicTestListG1, basicTestListG2, basicTestListG3 -- For testing.
    -- , basicTestListB1, basicTestListB2                  -- For testing.
    -- , basicTestDictionaryG1, basicTestDictionaryG2      -- For testing.
    -- , basicTestDictionaryB1, basicTestDictionaryB2      -- For testing.
    ) where

import Data.List hiding (splitAt)
import Prelude hiding (splitAt)
import qualified Data.Map as M

import Text.ParserCombinators.Parsec hiding ((<|>), many)
import Control.Applicative
import Control.Monad
import Text.Parsec.Number

import Data.Either
import Data.Maybe

import Utils


-- Test functions {{{1

basicTestIntegerG1 = "i42e"
basicTestIntegerG2 = "i-42e"
basicTestIntegerB1 = "i-42"

basicTestStringG1 = "4:spam"
basicTestStringG2 = "4:spammamam"
basicTestStringB1 = "4:spa"
basicTestStringB2 = "4aa:spam"

basicTestListG1 = "l4:spami42ee"
basicTestListG2 = "l3:onei1el3:twoi2eee"
basicTestListG3 = "l3:onei1ed3:onei-42e3:twoi42eee"
basicTestListB1 = "l3:oneiaee"
basicTestListB2 = "l3:onei1e"

basicTestDictionaryG1 = "d3:onei-42e3:twoi42ee"
basicTestDictionaryG2 = "d3:oned5:three4:spame3:twoi42ee"
basicTestDictionaryB1 = "d3:oned5:three4:spame3:twoi42e"
basicTestDictionaryB2 = "d3:oned5:three4:spame3:twoi42hee"

--}}}1



-- Types for "Bencode" parsing {{{1

{-|
    Data type represents general "map" between Bencode and Hakell data types.
-}
data BencodeData =
    BencodeInteger    TypeBencodeInteger
  | BencodeString     TypeBencodeString
  | BencodeList       TypeBencodeList
  | BencodeDictionary TypeBencodeDictionary
  deriving (Show, Read, Eq)

type TypeBencodeInteger        = Integer
type TypeBencodeString         = String
type TypeBencodeList           = [BencodeData]
type TypeBencodeDictionary     = M.Map String BencodeData
type TypeBencodeDictionaryElem = (String, BencodeData)



-- Parsing Bencode types {{{1

parseBencode :: String -> Either String BencodeData
parseBencode s =
    case parse (parseBencodeData <* eof) sourceName s of
        Left msg -> Left $ show msg
        Right x  -> Right x

    where sourceName = "Parsing bencode to basic haskell data types."



parseBencodeData :: Parser BencodeData
parseBencodeData =

        try parseBencodeDataInteger
    <|> try parseBencodeDataString
    <|> try parseBencodeDataList
    <|> try parseBencodeDataDictionary



parseInteger :: Parser TypeBencodeInteger
parseInteger = char 'i' *> int <* char 'e'

parseBencodeDataInteger :: Parser BencodeData
parseBencodeDataInteger = BencodeInteger <$> parseInteger



parseString :: Parser TypeBencodeString
parseString = do
    c <- decimal
    char ':'
    count (fromIntegral c) anyChar

parseBencodeDataString :: Parser BencodeData
parseBencodeDataString = BencodeString <$> parseString



parseList :: Parser TypeBencodeList
parseList = char 'l' *> many parseBencodeData <* char 'e'

parseBencodeDataList :: Parser BencodeData
parseBencodeDataList = BencodeList <$> parseList



parseDictionaryElem :: Parser TypeBencodeDictionaryElem
parseDictionaryElem = do
    key <- parseString
    val <- parseBencodeData
    return (key, val)

parseDictionary :: Parser [TypeBencodeDictionaryElem]
parseDictionary = char 'd' *> many parseDictionaryElem <* char 'e'

parseBencodeDataDictionary :: Parser BencodeData
parseBencodeDataDictionary = BencodeDictionary . M.fromList <$> parseDictionary

--}}}1

-- Composing Bencode types {{{1

composeBencode :: BencodeData -> String
composeBencode (BencodeInteger i)    = composeInteger i
composeBencode (BencodeString s)     = composeString s
composeBencode (BencodeList l)       = composeList l
composeBencode (BencodeDictionary d) = composeDictionary d



composeInteger :: TypeBencodeInteger -> String
composeInteger i = "i" ++ show i ++ "e"



composeString :: TypeBencodeString -> String
composeString s = show (length s) ++ ":" ++ s



composeList :: TypeBencodeList -> String
composeList l = "l" ++ composeList' l ++ "e"

composeList' :: [BencodeData] -> String
composeList' = foldl (\ s d -> s ++ composeBencode d) ""



composeDictionary :: TypeBencodeDictionary -> String
composeDictionary d = "d" ++ composeDictionary' d ++ "e"

composeDictionary' :: TypeBencodeDictionary -> String
composeDictionary' =
    foldl (\ s (k,v) -> s ++ keyStrGen k ++ composeBencode v) "" . M.toList
        where keyStrGen = composeBencode . BencodeString

-- }}}1
