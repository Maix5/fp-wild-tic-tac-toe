{-# LANGUAGE InstanceSigs #-}
module GameInfo where

import Debug.Trace

import Data.List
import Data.Maybe
import Data.Bifunctor

import Parse.Bencode
import Parse.TicTacToeMsg
import Parse.MsgPlusMap
import Utils hiding (fromJust)

import qualified Data.Matrix as MX



-- GameInfo {{{2
data GameInfo = GameInfo
    { playerName1   :: PlayerName
    , playerName2   :: PlayerName
    , moveNum       :: Integer -- Current move number, not index.
    , iGoFirst      :: Bool
    , xBoard        :: [TicTacToeMsgCoordinate]
    , oBoard        :: [TicTacToeMsgCoordinate]
    } deriving(Show)

type PlayerName = Maybe TicTacToeMsgId


emptyGameInfo :: Bool -> GameInfo
emptyGameInfo iStartedGame =
    GameInfo
        { playerName1 = Nothing
        , playerName2 = Nothing
        , moveNum     = 0
        , iGoFirst    = iStartedGame
        , xBoard      = []
        , oBoard      = []
        }


isMyTurn :: GameInfo -> Bool
isMyTurn gInfo
  | iGoFirst gInfo = odd $ moveNum gInfo
  | otherwise      = even $ moveNum gInfo

isOponentTurn :: GameInfo -> Bool
isOponentTurn = not . isMyTurn


getEvenPlayerName :: GameInfo -> PlayerName
getEvenPlayerName = playerName2
getOddPlayerName :: GameInfo -> PlayerName
getOddPlayerName = playerName1

currentPlayerName :: GameInfo -> PlayerName
currentPlayerName gInfo
  | even $ moveNum gInfo = getEvenPlayerName gInfo
  | odd $ moveNum gInfo  = getOddPlayerName gInfo
  | otherwise = Nothing

myName :: GameInfo -> PlayerName
myName gInfo
  | iGoFirst gInfo = getEvenPlayerName gInfo
  | otherwise      = getOddPlayerName gInfo
opponentName :: GameInfo -> PlayerName
opponentName gInfo
  | not $ iGoFirst gInfo = getEvenPlayerName gInfo
  | otherwise            = getOddPlayerName gInfo


setPlayer1Name :: TicTacToeMsgId -> GameInfo -> GameInfo
setPlayer1Name plName gInfo = gInfo { playerName1 = Just plName }
setPlayer2Name :: TicTacToeMsgId -> GameInfo -> GameInfo
setPlayer2Name plName gInfo = gInfo { playerName2 = Just plName }
setPlayerName :: TicTacToeMsgId -> Integer -> GameInfo -> GameInfo
setPlayerName plName move gInfo
  | odd move  = setPlayer1Name plName gInfo
  | even move = setPlayer2Name plName gInfo
  | otherwise = gInfo

--}}}2


-- Game board encoding {{{1

constructBoardModelMatrix :: [TicTacToeMsgCoordinate] -> [[Integer]]
constructBoardModelMatrix = chunks boardChunkSize . constructBoardModel

constructBoardModel :: [TicTacToeMsgCoordinate] -> [Integer]
constructBoardModel = foldr evaluateCoordinate emptyBoard

evaluateCoordinate :: TicTacToeMsgCoordinate -> [Integer] -> [Integer]
evaluateCoordinate c = setAt index 1
    where
        cordX = coordianteX c
        cordY = coordinateY c
        index = cordX + 3*cordY


boardChunkSize :: Integer
boardChunkSize = 3

boardSize :: Integer
boardSize = 9

emptyBoard :: [Integer]
emptyBoard
  | boardSize < 0 = []
  | otherwise     = [0 | _ <- [0..(boardSize - 1)]]

--}}}2

--}}}1

-- Checks {{{1

collisions :: [TicTacToeMsgCoordinate] -> [TicTacToeMsgCoordinate] -> Bool
collisions l = any (`elem` l)

isDublicate :: TicTacToeMsgCoordinate -> [TicTacToeMsgCoordinate] -> Bool
isDublicate = elem

rotateBoardL :: Integer -> [a] -> [a]
rotateBoardL n b = concat $ iterateN n rotateL $ chunks boardChunkSize b
rotateBoardR :: Integer -> [a] -> [a]
rotateBoardR n b = concat $ iterateN n rotateR $ chunks boardChunkSize b

--}}}1

gameInfoConstrIteration :: TicTacToeMsg -> GameInfo -> Either String GameInfo
gameInfoConstrIteration x prevG
  -- | trace ("constructGameInfo xBoard - " ++ show newXBoard) False
  --   = undefined
  -- | trace ("constructGameInfo oBoard - " ++ show newOBoard) False
  --   = undefined
  -- | trace ("constructGameInfo player - " ++ show pId) False
  --   = undefined
  -- | trace ("constructGameInfo prevPlayer - " ++ show prevPlayer) False
  --   = undefined
  -- | trace ("constructGameInfo player1 - " ++ show player1) False
  --   = undefined
  -- | trace ("constructGameInfo player2 - " ++ show player2) False
  --   = undefined

  | isDublicate pCoord prevXBoard || isDublicate pCoord prevOBoard
  = Left errDublicates
  | collisions newXBoard newOBoard
  = Left errCollisions
  -- | comparePl prevPlayer
  -- = Left errTwoTurn
  -- | playersExist && comparePlNot player1 && comparePlNot player2
  -- = Left errNoPlayer
  | otherwise = Right newPlInfo
    where
        errDublicates = errMsgWithVal pCoord "Coordinate duplicate."
        errCollisions = errMsgWithVal pCoord "Coordinate collision."
        errTwoPlayers = errMsg "Two players are required for the game."
        errNoPlayer   = errMsgWithVal pId "Player does not exist."
        errTwoTurn    = errMsgWithVal pId "Player made two turns at once."

        playersExist = isJust player1 && isJust player2

        pId    = msgId x
        pVal   = msgValue x
        pCoord = msgCoordinate x

        comparePl    = elem pId :: Maybe TicTacToeMsgId -> Bool
        comparePlNot = not . comparePl
        player1 = playerName1 prevG
        player2 = playerName2 prevG

        prevPlayer  = currentPlayerName prevG
        prevMoveNum = moveNum prevG
        prevXBoard  = xBoard prevG
        prevOBoard  = oBoard prevG

        newMoveNum    = prevMoveNum + 1
        newXBoard
          | pVal == X = pCoord : prevXBoard
          | otherwise = prevXBoard
        newOBoard
          | pVal == O = pCoord : prevOBoard
          | otherwise = prevOBoard
        newG = prevG
            { moveNum = newMoveNum
            , xBoard  = newXBoard
            , oBoard  = newOBoard
            }
        newPlInfo = setPlayerName pId newMoveNum newG

toGameInfo :: Bool -> [TicTacToeMsg] -> Either String GameInfo
toGameInfo iStartedGame tttMsg =
    toGameInfo' tttMsg $ emptyGameInfo iStartedGame

toGameInfo' :: [TicTacToeMsg] -> GameInfo -> Either String GameInfo
toGameInfo' tttMsg gInfo = foldl (\gi msg -> gameInfoConstrIteration msg =<< gi) (Right gInfo) tttMsg


fullBoard :: GameInfo -> Bool
fullBoard gInfo = length xB + length oB == fromIntegral boardSize
    where
        xB = xBoard gInfo
        oB = oBoard gInfo
