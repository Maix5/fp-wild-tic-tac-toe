{-# LANGUAGE InstanceSigs #-}
module ActionsWinner
    ( winner, winnerBasic
    , winnerCheck , winnerCheckName
    ) where

import Debug.Trace

import Data.List
import Data.Maybe
import Data.Bifunctor

import Parse.Bencode
import Parse.TicTacToeMsg
import Parse.MsgPlusMap
import Utils hiding (fromJust)

import GameInfo


-- Checks {{{1

hasRowAt :: Integer ->  [TicTacToeMsgCoordinate] -> Bool
hasRowAt i = hasHorizontalLine i . constructBoardModel

hasColAt :: Integer ->  [TicTacToeMsgCoordinate] -> Bool
hasColAt i = hasHorizontalLine i . rotateBoardR 1 . constructBoardModel

hasLine :: [Integer] -> Bool
hasLine board = sum board == boardChunkSize

hasHorizontalLine :: Integer ->  [Integer] -> Bool
hasHorizontalLine i board = hasLine line
    where
        startI  = boardChunkSize * i
        finishI = boardChunkSize * i + boardChunkSize
        line    = subList startI finishI board

hasMainDiagonal :: [TicTacToeMsgCoordinate] -> Bool
hasMainDiagonal = hasLine . mainDiagonal . constructBoardModelMatrix

hasNonMainDiagonal :: [TicTacToeMsgCoordinate] -> Bool
hasNonMainDiagonal = hasLine
                   . mainDiagonal
                   . chunks boardChunkSize
                   . rotateBoardR 1
                   . constructBoardModel


checkBoardForWinner :: [TicTacToeMsgCoordinate] -> Bool
checkBoardForWinner b =  any (`hasRowAt` b) count
                      || any (`hasColAt` b) count
                      || hasMainDiagonal b
                      || hasNonMainDiagonal b
                      where count = [0..(boardChunkSize - 1)]

--}}}1

testWithcollition = "d1:cd1:0i1e1:1i0ee2:id7:ceOtJgc4:prevd1:cd1:0i2e1:1i0ee2:id29:hTjysxZKCFVaSTeerrRRumEHPvyne4:prevd1:cd1:0i1e1:1i2ee2:id7:ceOtJgc4:prevd1:cd1:0i1e1:1i2ee2:id29:hTjysxZKCFVaSTeerrRRumEHPvyne1:v1:oe1:v1:xe1:v1:oe1:v1:xe"

constructGameInfo :: [TicTacToeMsg] -> GameInfo
                  -> Either String (Maybe TicTacToeMsgId)
constructGameInfo [] _     = Right Nothing
constructGameInfo (x:xs) prevG
  -- | trace ("constructGameInfo xBoard - " ++ show newXBoard) False
  --   = undefined
  -- | trace ("constructGameInfo oBoard - " ++ show newOBoard) False
  --   = undefined
  -- | trace ("constructGameInfo player - " ++ show pId) False
  --   = undefined
  -- | trace ("constructGameInfo prevPlayer - " ++ show prevPlayer) False
  --   = undefined
  -- | trace ("constructGameInfo player1 - " ++ show player1) False
  --   = undefined
  -- | trace ("constructGameInfo player2 - " ++ show player2) False
  --   = undefined

  | isDublicate pCoord prevXBoard || isDublicate pCoord prevOBoard
    = Left errDublicates
  | collisions newXBoard newOBoard       = Left errCollisions
  -- | comparePl prevPlayer = Left errTwoTurn
  -- | playersExist
  --   && comparePlNot player1
  --   && comparePlNot player2 = Left errNoPlayer
  | checkBoardForWinner newXBoard
    || checkBoardForWinner newOBoard = Right $ Just pId
  | otherwise = constructGameInfo xs newPlInfo
    where
        errDublicates = errMsgWithVal pCoord "Coordinate duplicate."
        errCollisions = errMsgWithVal pCoord "Coordinate collision."
        errTwoPlayers = errMsg "Two players are required for the game."
        errNoPlayer   = errMsgWithVal pId "Player does not exist."
        errTwoTurn    = errMsgWithVal pId "Player made two turns at once."

        playersExist = isJust player1 && isJust player2

        pId    = msgId x
        pVal   = msgValue x
        pCoord = msgCoordinate x

        comparePl    = elem pId :: Maybe TicTacToeMsgId -> Bool
        comparePlNot = not . comparePl
        player1 = playerName1 prevG
        player2 = playerName2 prevG

        prevPlayer  = currentPlayerName prevG
        prevMoveNum = moveNum prevG
        prevXBoard  = xBoard prevG
        prevOBoard  = oBoard prevG

        newMoveNum    = prevMoveNum + 1
        newXBoard
          | pVal == X = pCoord : prevXBoard
          | otherwise = prevXBoard
        newOBoard
          | pVal == O = pCoord : prevOBoard
          | otherwise = prevOBoard
        newG = prevG
            { moveNum = newMoveNum
            , xBoard  = newXBoard
            , oBoard  = newOBoard
            }
        newPlInfo = setPlayerName pId newMoveNum newG


winnerCheck :: GameInfo -> Bool
winnerCheck gInfo = checkBoardForWinner xB || checkBoardForWinner oB
    where
        xB = xBoard gInfo
        oB = oBoard gInfo


winnerCheckName :: GameInfo -> Maybe PlayerName
winnerCheckName gInfo
  | winnerCheck gInfo = Just $ currentPlayerName gInfo
  | otherwise         = Nothing


winnerBasic :: Bool -> [TicTacToeMsg] -> Either String (Maybe String)
winnerBasic iStartedGame interp = second (fmap show) gi
    where
        gi = constructGameInfo interp empGI
        empGI = emptyGameInfo iStartedGame

winner :: String -> Either String (Maybe String)
{-|
    Calculates if specified "Wild Tic-Tac-Toe" game encoded in
    **Bencode** string defined a winner or not.

    Returns either errorMsg or maybe a winner's name.
-}
winner s = do
    interp <- interpretateMsg $ parseBencode s
    second (fmap show) $ constructGameInfo interp $ emptyGameInfo True


