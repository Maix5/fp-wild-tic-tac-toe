{-# LANGUAGE OverloadedStrings #-}

module Communication.HttpTest where

import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Data.ByteString.Char8 as S8
import           Network.HTTP.Simple

import Test.TestData


-- Tutorial:
-- https://haskell-lang.org/library/http-client


httpTest :: IO ()
httpTest = do
    response <- httpLBS "http://httpbin.org/get"

    putStrLn $ "The status code was: " ++
        show (getResponseStatusCode response)
    print $ getResponseHeader "Content-Type" response
    L8.putStrLn $ getResponseBody response


httpTest1 s = httpLBS r >>= print . getResponseHeaders
    where r = setRequestHost s defaultRequest
httpTest2 s = httpLBS r >>= print . getResponseStatusCode
    where r = setRequestHost s defaultRequest
httpTest3 s = httpLBS r >>= L8.putStrLn . getResponseBody
    where r = setRequestHost s defaultRequest




testHttpPost' :: String -> IO ()
testHttpPost' gameId = do
    let request = setRequestMethod "POST" 
            $ setRequestHost "tictactoe.haskell.lt"
            $ setRequestPath (S8.pack $ "/game/" ++ gameId ++ "/player/1") -- Use Strick bytestring
            $ addRequestHeader "Content-Type" "application/bencode+map"
            $ setRequestBodyLBS (L8.pack testBasicData)
            $ setRequestPort 80 
            defaultRequest

    print request

    response <- httpLBS request

    putStrLn $ "The status code was: " ++
        show (getResponseStatusCode response)
    print $ getResponseHeader "Content-Type" response
    L8.putStrLn $ getResponseBody response


testHttpGet' :: String -> IO ()
testHttpGet' gameId = do
    let request = setRequestMethod "GET"
            $ setRequestHost "tictactoe.haskell.lt"
            $ setRequestPath (S8.pack $ "/game/" ++ gameId ++ "/player/2")
            $ addRequestHeader "Accept" "application/bencode+map"
            $ setRequestPort 80
            defaultRequest

    print request
    response <- httpLBS request

    putStrLn $ "The status code was: " ++
        show (getResponseStatusCode response)
    print $ getResponseHeader "Content-Type" response
    L8.putStrLn $ getResponseBody response




testHttpPost :: String -> IO ()
testHttpPost gameId = do
    request' <- parseRequest
        ("http://tictactoe.haskell.lt/game/" ++ gameId ++ "/player/1")
    let request = setRequestMethod "POST"
            $ addRequestHeader "Content-Type" "application/bencode+map"
            $ setRequestBodyLBS (L8.pack testBasicData)
            $ setRequestPort 80 request'

    print request

    response <- httpLBS request

    putStrLn $ "The status code was: " ++
        show (getResponseStatusCode response)
    print $ getResponseHeader "Content-Type" response
    L8.putStrLn $ getResponseBody response


testHttpGet :: String -> IO ()
testHttpGet gameId = do
    request' <- parseRequest
        ("http://tictactoe.haskell.lt/game/" ++ gameId ++ "/player/2")
    let request = setRequestMethod "GET"
            $ addRequestHeader "Accept" "application/bencode+map"
            $ setRequestPort 80 request'

    print request

    response <- httpLBS request

    putStrLn $ "The status code was: " ++
        show (getResponseStatusCode response)
    print $ getResponseHeader "Content-Type" response
    L8.putStrLn $ getResponseBody response
