{-# LANGUAGE OverloadedStrings #-}

module Server.ScottyTest where

import Web.Scotty

import Data.Monoid (mconcat)

main = scotty 3000 $ do
    get "/:word" $ do
        beam <- param "word"
        html $ mconcat ["<h1>Scotty, ", beam, " me up!</h1>"]
    post "/:word" $ do
        beam <- param "word"
        raw beam
