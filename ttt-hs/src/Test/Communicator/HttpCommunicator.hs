module Test.Communicator.HttpCommunicator where


import Communication.HttpCommunicator
import Test.TestData
import Utils


testHttpRequest1Data gameId = HttpRequest
    { httpRequestHost       = "tictactoe.haskell.lt"
      , httpRequestPort       = 80
      , httpRequestMsgFormat  = BencodeWithMap
      , httpRequestGameId     = gameId
      , httpRequestPlayerName = Player1
      , httpRequestMsg        = testBasicData
    }


testHttpRequest2Data gameId = HttpRequest
    { httpRequestHost       = "tictactoe.haskell.lt"
      , httpRequestPort       = 80
      , httpRequestMsgFormat  = BencodeWithMap
      , httpRequestGameId     = gameId
      , httpRequestPlayerName = Player1
      , httpRequestMsg        = testBasicData
    }


testHttpComunicatorPost gameId = httpPost $ testHttpRequest1Data gameId
testHttpComunicatorGet gameId  = httpGet $ testHttpRequest2Data gameId


testPostGet gameId = testHttpComunicatorPost gameId >> testHttpComunicatorGet gameId

-- testPostGetComare gameId = testPostGet gameId >>= (\x -> httpResponseMsg . fromRight
