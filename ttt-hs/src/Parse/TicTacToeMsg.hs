{-# LANGUAGE InstanceSigs #-}

module Parse.TicTacToeMsg
    ( TicTacToeMsg(..)
    , TicTacToeMsgId(..), TicTacToeMsgValue(..), TicTacToeMsgCoordinate(..)
    , keyMsgPrevious, keyMsgCoordinate, keyMsgValue, keyMsgId
    , updateTicTacToeMsg
    ) where




data TicTacToeMsg = TicTacToeMsg
    { msgId        :: TicTacToeMsgId
    , msgValue     :: TicTacToeMsgValue
    , msgCoordinate :: TicTacToeMsgCoordinate
    } deriving (Show)





-- TicTacToe messages properties types {{{1
newtype TicTacToeMsgId = UserId String
    deriving(Eq)

instance Show TicTacToeMsgId where
    show :: TicTacToeMsgId -> String
    show (UserId s) = s



data TicTacToeMsgCoordinate = Coordinate
    { coordianteX :: Integer
    , coordinateY :: Integer
    } deriving(Eq)

instance Show TicTacToeMsgCoordinate where
    show :: TicTacToeMsgCoordinate -> String
    show c = show (coordianteX c, coordinateY c)



data TicTacToeMsgValue = X | O
    deriving(Show, Eq)
--}}}1


-- TicTacToe messages keys {{{1
keyMsgPrevious   = "prev"
keyMsgCoordinate = "c"
keyMsgValue      = "v"
keyMsgId         = "id"
-- }}}1


updateTicTacToeMsg :: [TicTacToeMsg] -> TicTacToeMsg -> [TicTacToeMsg]
updateTicTacToeMsg ms m = ms ++ [m]
