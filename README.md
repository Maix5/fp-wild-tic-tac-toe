# "Wild Tic-Tac-Toe" game

This project implements "[**Wild Tic-Tac-Toe**](https://en.wikipedia.org/wiki/Wild_tic-tac-toe)"
game in **haskell**.

All other project requirements can be found
[here](https://git.mif.vu.lt/vipo/FP.git).


This project implements both **server** and **client**. Server can host any number of games initialized by the client.

At current state communication between server and client uses **Bencode** protocol in which any list is represented by a dictionary.


## Requirements

Note that this is **stack-haskell-project**.
So to easily build this project
**[stack](https://docs.haskellstack.org/en/stable/README/)**
haskell build tool needs to be installed.


## How to run!

The simplest way to build ad run project is to use
*Makefile* or *stack* build tool directly.

P.S. In case of incorrect cmd options occurrence program will display help
message with possible cmd options.

### Server

Execute in server mode with `s` flag.
```bash
$ make exec args="s <options>"`
```

Server specific options:
```
 * [<port>]
```

Server has several resources to use:

* `<host>:<port>/history` --- Displays performed games histories list.
* `<host>:<port>/history/:gameId` --- Displays specified id game moves history.

P.S. For more in dept description of possible resources look at
[source code](/ttt-hs/src/Server/TicTacToe.hs)
(available at `/ttt-hs/src/Server/TicTacToe.hs`).


### Client

Execute in client mode with `c` flag.
```bash
$ make exec args="c <options>"`
```

Client specific options:
```
 * <gameId> [<host>] [<port>]
```



## Credits

Project icon: [Tic Tac Toe](http://thenounproject.com/term/tic-tac-toe/213664/)
icon by [Prasad](https://thenounproject.com/designerPrasad/)
from [the Noun Project](http://thenounproject.com/).
