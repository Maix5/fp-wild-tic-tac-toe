{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs      #-}
{-# LANGUAGE OverloadedStrings #-}

module Communication.HttpCommunicator
    ( HttpRequest(..)
    , setHttpRequestHost, setHttpRequestPort, setHttpRequestMsgFormat
    , setHttpRequestGameId, setHttpRequestPlayerName, setHttpRequestMsg
    , TicTacToeMsgFormat(..), TicTacToePlayer(..)
    , HttpResponse(..)
    , setHttpResponseCode, setHttpResponseMsg
    , httpPost, httpGet
    , getHttpMessageContentsOnly
    )where

import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Data.ByteString.Char8 as S8
import           Network.HTTP.Simple
import           Network.HTTP.Conduit
import           Network.HTTP.Client
import Text.Printf
import qualified Data.Text as T


getConnectionManager =
    newManager defaultManagerSettings
        {managerResponseTimeout = responseTimeoutNone}
        -- {managerResponseTimeout = responseTimeoutMicro 1000000000000}

getHttpMessageContentsOnly :: HttpResponse -> String
getHttpMessageContentsOnly resp = msg
    where
        basicMsg = T.pack $ httpResponseMsg resp
        msg = T.unpack $ T.dropAround (== '"') $ T.dropAround (/= '"') basicMsg


-- Http Request {{{1
data HttpRequest = HttpRequest
    { httpRequestHost       :: String
    , httpRequestPort       :: Int
    , httpRequestMsgFormat  :: TicTacToeMsgFormat
    , httpRequestGameId     :: String
    , httpRequestPlayerName :: TicTacToePlayer
    , httpRequestMsg        :: String
    } deriving(Show)

setHttpRequestHost x httpReq       = httpReq {httpRequestHost       = x}
setHttpRequestPort x httpReq       = httpReq {httpRequestPort       = x}
setHttpRequestMsgFormat x httpReq  = httpReq {httpRequestMsgFormat  = x}
setHttpRequestGameId x httpReq     = httpReq {httpRequestGameId     = x}
setHttpRequestPlayerName x httpReq = httpReq {httpRequestPlayerName = x}
setHttpRequestMsg x httpReq        = httpReq {httpRequestMsg        = x}





data TicTacToeMsgFormat =  BencodeWithMap

instance Show TicTacToeMsgFormat where
    show :: TicTacToeMsgFormat -> String
    show BencodeWithMap = "application/bencode+map"


data TicTacToePlayer = Player1 | Player2

instance Show TicTacToePlayer where
    show :: TicTacToePlayer -> String
    show Player1 = "player/1"
    show Player2 = "player/2"

-- }}}1


-- Http Response {{{1
data HttpResponse = HttpResponse
    { httpResponseCode :: Int
    , httpResponseMsg  :: String
    } deriving(Show)

setHttpResponseCode x httpResp = httpResp {httpResponseCode = x}
setHttpResponseMsg x httpResp  = httpResp {httpResponseMsg  = x}
-- }}}1


-- Utility functions {{{1
convStrS  = S8.pack
convStrL  = L8.pack


httpRequestPath :: String -> TicTacToePlayer -> String
httpRequestPath gameId player = printf "/game/%s/%s" gameId $ show player


validateHttpResponse :: Response L8.ByteString
                     -> Either HttpResponse HttpResponse
validateHttpResponse response
  | code == 200 = Right response'
  | otherwise   = Left response'
    where
        code = getResponseStatusCode response
        msg  = show $ getResponseBody response

        response'              = HttpResponse
            { httpResponseCode = code
            , httpResponseMsg  = msg
            }

-- }}}1


-- Main functions {{{1

httpPost :: HttpRequest -> IO (Either HttpResponse HttpResponse)
httpPost httpReq = do
    manager <- getConnectionManager
    let request = setRequestMethod "POST"
                $ setRequestHost host
                $ setRequestPath path
                $ addRequestHeader "Content-Type" msgFormat
                $ setRequestBodyLBS msg
                $ setRequestPort port
                $ setRequestManager manager
                defaultRequest
    -- print request -- Checking constructed Request.
    response <- httpLBS request
    return $ validateHttpResponse response
    where
        host      = convStrS $ httpRequestHost httpReq
        port      = httpRequestPort httpReq
        msgFormat = convStrS $ show $ httpRequestMsgFormat httpReq
        gameId    = httpRequestGameId httpReq
        player    = httpRequestPlayerName httpReq
        msg       = convStrL $ httpRequestMsg httpReq
        path      = convStrS $ httpRequestPath gameId player


httpGet :: HttpRequest -> IO (Either HttpResponse HttpResponse)
httpGet httpReq = do
    manager <- getConnectionManager
    let request = setRequestMethod "GET"
                $ setRequestHost host
                $ setRequestPath path
                $ addRequestHeader "Accept" msgFormat
                $ setRequestBodyLBS msg
                $ setRequestPort port
                $ setRequestManager manager
                defaultRequest
    -- print request -- Checking constructed Request.
    response <- httpLBS request
    return $ validateHttpResponse response
    where
        host      = convStrS $ httpRequestHost httpReq
        port      = httpRequestPort httpReq
        msgFormat = convStrS $ show $ httpRequestMsgFormat httpReq
        gameId    = httpRequestGameId httpReq
        player    = httpRequestPlayerName httpReq
        msg       = convStrL $ httpRequestMsg httpReq
        path      = convStrS $ httpRequestPath gameId player

-- }}}1
