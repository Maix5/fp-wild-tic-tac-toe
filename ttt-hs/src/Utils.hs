module Utils where


import Prelude hiding (splitAt)
import Data.List hiding (splitAt)


readMaybe :: (Read a) => String -> Maybe a
{-|
   Safe read function.
-}
readMaybe s = case reads s of
                [(x,"")] -> Just x
                _        -> Nothing


splitAt :: Int -> [a] -> ([a],[a])
splitAt n l = (take n l, drop n l)


splitByFirstWithSeparator :: (a -> Bool) -> [a] -> ([a], [a], [a])
{-|
    Split list by the **first** element,
    identified by predicate.
    Separator is included as second tuple element.
-}
splitByFirstWithSeparator p x = (x1, x2, x3)
    where
        (x1, x3') = break p x
        (x2, x3) = splitAt 1 x3'

splitByFirst :: (a -> Bool) -> [a] -> ([a], [a])
{-|
    Split list by the **first** element,
    identified by predicate.
-}
splitByFirst p x = (x1, x3)
    where
        (x1, _, x3) = splitByFirstWithSeparator p x


extrByFirstSeparatorStrick :: Char -> String -> Maybe (String, String)
{-|
    Split list by the **first** element,
    identified by predicate.
    Afterwards checks **if any** separator occurred in the list.
-}
extrByFirstSeparatorStrick sp s
  | not $ null separator = Just (val, rest)
  | otherwise            = Nothing
    where
        (val, separator, rest) = splitByFirstWithSeparator (== sp) s


errMsg :: String -> String
{-|
    Construct simple error message.
-}
errMsg msg = "Error: " ++ msg

errMsgWithVal :: Show a => a -> String -> String
{-|
    Construct simple error message with specific object value.
-}
errMsgWithVal obj msg = "Error: " ++ show obj ++ " - " ++ msg



takeByIndex :: Integer -> [a] -> Maybe a
{-|
    Takes element at specified index.
-}
takeByIndex i l = case drop (fromInteger i) l of
                    (x:_) -> Just x
                    _     -> Nothing


infix 4 !!?

(!!?) :: [a] -> Integer -> Maybe a
l !!? i = takeByIndex i l


fromJust :: Maybe a -> a
fromJust (Just x) = x
fromJust _ = error "Maybe.fromJust: Nothing"


fromLeft :: Either a b -> a
fromLeft (Left x) = x
fromLeft _ = error "Either.fromLeft: Right Value"

fromRight :: Either a b -> b
fromRight (Right x) = x
fromRight _ = error "Either.fromRight: Left Value"



rotateL :: [[a]] -> [[a]]
{-|
    Rotates matrix 90 degrees counter clock vise.
-}
rotateL = reverse . transpose
rotateR :: [[a]] -> [[a]]
{-|
    Rotates matrix 90 degrees clock vise.
-}
rotateR = transpose . reverse


iterateN :: Integer -> (a -> a) -> a -> a
{-|
    Executes function composition N times.
-}
iterateN n f a = iterate f a !! fromIntegral n

chunks :: Integer -> [a] -> [[a]]
{-|
    Divides list info list of "chunks".
-}
chunks n = takeWhile (not . null) . unfoldr (Just . splitAt (fromIntegral n))
-- chunks _ [] = []
-- chunks n xs = ys : chunks n zs
--     where (ys, zs) = splitAt n xs

setAt :: Integer -> a -> [a] -> [a]
{-|
    Generates a new list with n element replaced.
-}
setAt n x xs = x1 ++ (x:x2)
    where (x1, _:x2) = splitAt (fromIntegral n) xs

(.->) :: (Integer, a) -> [a] -> [a]
(n, x) .-> xs = setAt n x xs


subList :: Integer -> Integer -> [a] -> [a]
subList stI fnI = drop (fromIntegral stI) . take (fromIntegral fnI)

mainDiagonal :: [[a]] -> [a]
mainDiagonal xs = zipWith (!!) xs [0..]


{-|
   Lists union intersection difference.
-}
listsUnionIntersDiff :: Eq a=> [a] -> [a] -> [a]
a `listsUnionIntersDiff` b = (a `union` b) \\ (a `intersect` b)
