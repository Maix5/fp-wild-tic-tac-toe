{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances    #-}
-- {-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE InstanceSigs         #-}

module Parse.MsgPlusMap
    ( interpretateMsg
    , interpretateMsgFromStr

    , obscureMsg

    -- , interpretateMsg'         -- For testing
    -- , BencodeDataExtracter(..) -- For testing
    -- , testMsg1, testMsg2       -- For testing

    , testObscurator1 -- For testing
    , testObscurator2 -- For testing
    , testData
    ) where


import Debug.Trace

import qualified Data.Map as M
import Data.List

import Data.Either
import Data.Maybe
import Data.Char

import Parse.TicTacToeMsg
import Parse.Bencode
import Utils


-- Test functions {{{1

testMsg1="d1:cd1:0i1e1:1i2ee2:id1:q4:prevd1:cd1:0i0e1:1i1ee2:id1:q1:v1:oe1:v1:xe"
testMsg2="d1:cd1:0i1e1:1i2ee2:id24:vfhDyBivlCqMsFOKackKWFfR4:prevd1:cd1:0i0e1:1i2ee2:id27:SEbCFfojtWaoONrWBvIQffTuOaz4:prevd1:cd1:0i1e1:1i1ee2:id24:vfhDyBivlCqMsFOKackKWFfR4:prevd1:cd1:0i0e1:1i1ee2:id27:SEbCFfojtWaoONrWBvIQffTuOaz4:prevd1:cd1:0i2e1:1i2ee2:id24:vfhDyBivlCqMsFOKackKWFfR4:prevd1:cd1:0i0e1:1i0ee2:id27:SEbCFfojtWaoONrWBvIQffTuOaz1:v1:oe1:v1:oe1:v1:oe1:v1:xe1:v1:oe1:v1:xe"
testMsg3="d1:cd1:0i0e1:1i0ee2:id23:rLUyXpZHJSBLAgKpyMSthyL4:prevd1:cd1:0i2e1:1i0ee2:id23:rLUyXpZHJSBLAgKpyMSthyL1:v1:oe1:v1:xe"

--}}}1

-- Main TTT message parser logic {{{1
class BencodeDataExtracter a where
    fromBencodeData :: BencodeData -> Maybe a
    toBencodeData :: a -> BencodeData


-- Misc interpreter {{{2
instance BencodeDataExtracter TypeBencodeInteger where
    fromBencodeData (BencodeInteger i) = Just i
    fromBencodeData _                  = Nothing

    toBencodeData = BencodeInteger

instance BencodeDataExtracter TypeBencodeString where
    fromBencodeData (BencodeString s) = Just s
    fromBencodeData _                 = Nothing

    toBencodeData = BencodeString

instance BencodeDataExtracter TypeBencodeList where
    fromBencodeData (BencodeList l) = Just l
    fromBencodeData _               = Nothing

    toBencodeData = BencodeList

instance BencodeDataExtracter TypeBencodeDictionary where
    fromBencodeData (BencodeDictionary d) = Just d
    fromBencodeData _                     = Nothing

    toBencodeData = BencodeDictionary
--}}}2

-- Msg coordinates interpreter {{{2

instance BencodeDataExtracter TicTacToeMsgCoordinate where
    fromBencodeData :: BencodeData -> Maybe TicTacToeMsgCoordinate
    fromBencodeData b = do
        x <- fromBencodeData =<< takeByIndex 0 =<< sortedCoordVector
        y <- fromBencodeData =<< takeByIndex 1 =<< sortedCoordVector
        return $ Coordinate x y
            where
                sortVector = map snd . M.toList
                coordMap   = M.lookup keyMsgCoordinate =<< fromBencodeData b
                sortedCoordVector
                  = sortVector
                  <$> (coordMap >>= fromBencodeData
                      :: Maybe TypeBencodeDictionary)

    toBencodeData :: TicTacToeMsgCoordinate -> BencodeData
    toBencodeData (Coordinate x y) = BencodeDictionary mapC
        where xC = toBencodeData x
              yC = toBencodeData y
              mapC = M.insert "0" xC $ M.insert "1" yC M.empty
--}}}2

-- Msg value interpreter {{{2
instance Read TicTacToeMsgValue where
    readsPrec :: Int -> ReadS TicTacToeMsgValue
    readsPrec _ (c:cs)
      | cH == 'X' = [(X, cs)]
      | cH == 'O' = [(O, cs)]
        where cH = toUpper c
    readsPrec _ _ = []

instance BencodeDataExtracter TicTacToeMsgValue where
    fromBencodeData :: BencodeData -> Maybe TicTacToeMsgValue
    fromBencodeData m
      = fromBencodeData m
      >>= M.lookup keyMsgValue >>= fromBencodeData >>= readMaybe

    toBencodeData :: TicTacToeMsgValue -> BencodeData
    toBencodeData X = BencodeString "x"
    toBencodeData O = BencodeString "o"
--}}}2

-- Msg id interpretater {{{2
instance BencodeDataExtracter TicTacToeMsgId where
    fromBencodeData :: BencodeData -> Maybe TicTacToeMsgId
    fromBencodeData m
      = UserId
      <$> (fromBencodeData m >>= M.lookup keyMsgId >>= fromBencodeData)

    toBencodeData :: TicTacToeMsgId -> BencodeData
    toBencodeData (UserId s) = BencodeString s
--}}}2

-- Over all msg interpreter {{{2
instance BencodeDataExtracter TicTacToeMsg where
    fromBencodeData :: BencodeData -> Maybe TicTacToeMsg
    fromBencodeData b = do
        id         <- fromBencodeData b
        coordinate <- fromBencodeData b
        value      <- fromBencodeData b
        return TicTacToeMsg
            { msgId         = id
            , msgCoordinate = coordinate
            , msgValue      = value
            }

    toBencodeData :: TicTacToeMsg -> BencodeData
    toBencodeData tttMsg= bencodeMap
        where
            id = toBencodeData $ msgId tttMsg
            val = toBencodeData $ msgValue tttMsg
            coords = toBencodeData $ msgCoordinate tttMsg

            bencodeMap = toBencodeData
                       $ M.insert keyMsgId id
                       $ M.insert keyMsgValue val
                       $ M.insert keyMsgCoordinate coords
                       M.empty

--}}}2

--}}}1

-- Interpretating msg {{{1
interpretateMsg :: Either String BencodeData -> Either String [TicTacToeMsg]
interpretateMsg (Left m) = Left m
interpretateMsg (Right b) =
    case interpretateMsg' (Just b) [] of
      -- Just bList -> Right bList
      Just bList -> Right $ reverse bList
      Nothing    -> Left $ errMsg "Could not interpretate bencoded message."

interpretateMsgFromStr :: String -> Either String [TicTacToeMsg]
interpretateMsgFromStr = interpretateMsg . parseBencode


interpretateMsg' :: Maybe BencodeData -> [TicTacToeMsg] -> Maybe [TicTacToeMsg]
interpretateMsg' Nothing res = Just res
interpretateMsg' (Just b@(BencodeDictionary d)) res
  | M.null d = Just res
  | otherwise = do
    bMsg <- fromBencodeData b
    interpretateMsg' prev (bMsg : res)
        where prev = M.lookup keyMsgPrevious =<< fromBencodeData b
interpretateMsg' _ _ = Nothing
--}}}1

-- Obscure msg {{{1
obscureMsg :: [TicTacToeMsg] -> BencodeData
obscureMsg = obscureMsg' -- . reverse

obscureMsg' :: [TicTacToeMsg] -> BencodeData
obscureMsg' []     = BencodeDictionary M.empty
obscureMsg' (m:ms) = insertPrevMsg prevMsgB msgB
    where
        msgB     = toBencodeData m
        prevMsgB = obscureMsg' ms

insertPrevMsg :: BencodeData -> BencodeData -> BencodeData
insertPrevMsg prevMsg currentMsg@(BencodeDictionary currentMsgMap)
  -- | trace ("\n") False = undefined
  -- | trace ("---------------------\n") False = undefined
  -- | trace ("Map - " ++ show currentMsgMap) False = undefined
  -- | trace ("---------------------\n") False = undefined
  | (not . checkNullDictionary) prevMsg =
      BencodeDictionary $ M.insert keyMsgPrevious prevMsg currentMsgMap
  | otherwise = currentMsg
insertPrevMsg _ currentMsg = currentMsg

checkNullDictionary :: BencodeData -> Bool
checkNullDictionary (BencodeDictionary msgMap) = null msgMap
checkNullDictionary _ = False

-- }}}1

testData = "d1:cd1:0i1e1:1i1ee2:id1:B4:prevd1:cd1:0i2e1:1i1ee2:id1:A1:v1:oe1:v1:xe"

testObscurator1 s = do
    interp <- interpretateMsg $ parseBencode s
    Right $ obscureMsg interp

testObscurator2 s = do
    interp <- interpretateMsg $ parseBencode s
    interpretateMsg $ Right $ obscureMsg interp
