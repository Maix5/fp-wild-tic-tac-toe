module Test.Parse.Bencode
    ( prop_bencodeDataComposingInversible
    , BencodeData(..)
    , arbitrary
    )where

import Test.QuickCheck
import Data.Either
import qualified Data.Map as M

import Parse.Bencode


-- prop_commutativeAdd :: Int -> Int -> Bool
-- prop_commutativeAdd x y =
--     x + y == y + x

prop_bencodeDataComposingInversible :: BencodeData -> Bool
prop_bencodeDataComposingInversible x =
    either (const False) (== x) $ parseBencode $ composeBencode x



genBenI = BencodeInteger <$> arbitrary
genBenS = BencodeString  <$> arbitrary


genSizedBenL :: Int -> Gen BencodeData
genSizedBenL 0 = BencodeList <$> listOf (oneof [genBenI, genBenS])
genSizedBenL n = do
    i <- genBenI
    s <- genBenS
    l <- BencodeList <$> resize 2 (listOf $ genSizedBenL (n - 1))
    d <- resize 2 $ sized genSizedBenD
    BencodeList <$> listOf (elements [i, s, l, d])


genSizedBenListD :: Int -> Gen [(String, BencodeData)]
genSizedBenListD 0 = do
    k <- listOf arbitrary
    v <- listOf $ oneof [genBenI, genBenS]
    return $ zip k v

genSizedBenListD n = do
    let
        i = genBenI
        s = genBenS
        l = resize 2 $ sized genSizedBenL
        d = (BencodeDictionary . M.fromList)
            <$> resize 2 (genSizedBenListD (n - 1))

    k <- listOf arbitrary
    v <- listOf $ oneof [i, s, l, d]
    return $ zip k v


genSizedBenD :: Int -> Gen BencodeData
genSizedBenD n = do
    d <- genSizedBenListD n
    return $ BencodeDictionary $ M.fromList d


genBencodeData :: Gen BencodeData
genBencodeData = do
    i <- genBenI
    s <- genBenS
    l <- sized genSizedBenL
    d <- sized genSizedBenD
    elements [i, s, l, d]

instance Arbitrary BencodeData where
    arbitrary = genBencodeData
