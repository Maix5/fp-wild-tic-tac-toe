-- Main module for TicTacToe Client execution.
module Client.TicTacToe (main, mainDefault) where

import Debug.Trace

import Data.Maybe
import Data.Either
import Control.Monad


import GameInfo
import ActionsWinner
import ActionsMove
import Parse.Bencode
import Parse.TicTacToeMsg
import Parse.MsgPlusMap
import Utils
import Communication.HttpCommunicator
import Test.Communicator.HttpCommunicator

defaultPlayer1Name = "1"
defaultPlayer2Name = "2"

defaultHost = "localhost"
defaultPort = 3000

defaultHttpRequestData =
    HttpRequest
        { httpRequestHost         = defaultHost
        , httpRequestPort       = defaultPort
        , httpRequestMsgFormat  = BencodeWithMap
        , httpRequestGameId     = ""
        , httpRequestPlayerName = Player1
        , httpRequestMsg        = ""
        }

defaultFirstTrunFlag = True


testPlayer1GameCfg gameId =
    GameCfg
        { firstTurn = True
        , cfgHost = defaultHost
        , cfgPort = defaultPort
        , cfgGameId = gameId
        }

testPlayer2GameCfg gameId =
    GameCfg
        { firstTurn = False
        , cfgHost = defaultHost
        , cfgPort = defaultPort
        , cfgGameId = gameId
        }


helpText s = s ++ " : <host> <port> <gameId> ["++ firstFlag ++"]"
firstFlag = "fst"



mainDefault :: String -> IO ()
mainDefault = main defaultHost defaultPort


main :: String -> Int -> String -> IO ()
main host port gameId = do
    let gCfg = GameCfg { firstTurn = True , cfgHost   = host , cfgPort   = port , cfgGameId = gameId }

    displayMsg "Game Started"
    iniciateGame gCfg
    displayMsg "Game finished"





data GameCfg = GameCfg
    { firstTurn :: Bool
      , cfgHost    :: String
      , cfgPort    :: Int
      , cfgGameId  :: String
    } deriving(Show)


iniciateGame :: GameCfg -> IO ()
iniciateGame gCfg
  | firstTurn gCfg = firstPlay gCfg req
  | otherwise      = void $ playGame gCfg req
    where
        req = constructHttpRequest gCfg


firstPlay :: GameCfg -> HttpRequest -> IO ()
firstPlay gCfg req = do
    gInfo       <- getGameState gCfg []
    -- playGamePost req gCfg []
    playGame gCfg req
    return ()


playGame :: GameCfg -> HttpRequest -> IO ()
playGame gCfg req = playGame' gCfg req Nothing []

playGame' :: GameCfg
          -> HttpRequest
          -> Maybe GameInfo
          -> [TicTacToeMsg]
          -> IO ()
playGame' gCfg req Nothing gameHistory = playGame'' gCfg req gameHistory

playGame' gCfg req (Just gInfo) gameHistory
  -- | trace ("Check winner - " ++ show (winnerCheck gInfo)) False = undefined
  -- | trace ("Check full Board - " ++ show (fullBoard gInfo)) False = undefined
  -- | trace ("Check game info - " ++ show gInfo) False = undefined
  | winnerCheck gInfo || fullBoard gInfo = handleGameEnd gCfg gameHistory
  | otherwise = playGame'' gCfg req gameHistory

playGame'' :: GameCfg -> HttpRequest -> [TicTacToeMsg] -> IO ()
playGame'' gCfg req gameHistory = do
    -- ToDo rewite algorithm to use post method only and receive new history.
    gameHistory  <- playGamePost req gCfg gameHistory
    gi           <- getGameState gCfg gameHistory
    if winnerCheck gi || fullBoard gi
       then handleGameEnd gCfg gameHistory
       else playGame' gCfg req (Just gi) gameHistory


handleGameEnd :: GameCfg -> [TicTacToeMsg] -> IO ()
handleGameEnd gCfg gameHistory =
    case winnerBasic (firstTurn gCfg) gameHistory of
        Left err -> fail err
        Right maybeName ->

            case maybeName of

                Just name -> do

                    displaySeparator
                    displayMsg ("WON player: " ++ name)
                    displaySeparator

                Nothing -> do
                    displaySeparator
                    displayMsg "DRAW"
                    displaySeparator


getGameState :: GameCfg -> [TicTacToeMsg] -> IO GameInfo
getGameState gCfg gameHistory = do
    let gInfo = toGameInfo (firstTurn gCfg) gameHistory

    case gInfo of

        Left err -> fail err
        Right gI -> return gI


displaySeparator :: IO ()
displaySeparator = displayMsg $ replicate 80 '-'

displayGameState :: GameCfg -> [TicTacToeMsg] -> IO ()
displayGameState gCfg gameHistory =
    print =<< getGameState gCfg gameHistory

displayMsg :: String -> IO ()
displayMsg = putStrLn


constructHttpRequest :: GameCfg -> HttpRequest
constructHttpRequest gCfg
  | firstTurn gCfg = setHttpRequestPlayerName Player1 req
  | otherwise      = setHttpRequestPlayerName Player2 req

    where
        h   = cfgHost gCfg
        p   = cfgPort gCfg
        gId = cfgGameId gCfg
        req =
            setHttpRequestHost h
            $ setHttpRequestPort p
            $ setHttpRequestGameId gId
            defaultHttpRequestData

getPlayerName :: GameCfg -> TicTacToeMsgId
getPlayerName gCfg
  | firstTurn gCfg = UserId defaultPlayer1Name
  | otherwise      = UserId defaultPlayer2Name



playGamePost :: HttpRequest -> GameCfg -> [TicTacToeMsg] -> IO [TicTacToeMsg]
playGamePost req gCfg gameHistory =
    case  moveBasic (firstTurn gCfg) gameHistory of

        Left err            -> fail err
        Right (val, coords) -> do

            let tttMsg =
                    TicTacToeMsg
                        { msgId = getPlayerName gCfg
                        , msgValue      = val
                        , msgCoordinate = coords
                        }
            let newHistory     = updateTicTacToeMsg gameHistory tttMsg
            let encodedHistory = composeBencode $ obscureMsg newHistory
            let req'            = setHttpRequestMsg encodedHistory req

            displaySeparator
            displayMsg " Performed move "
            displaySeparator
            displayGameState gCfg newHistory
            displaySeparator

            -- displayMsg "------------ POST --------------"
            -- print req
            resp <- httpPost req'
            case resp of
                Left err -> fail $ show err
                Right resp -> do

                    respGameHistory <- interpretatingMsgFromResponse resp

                    displaySeparator
                    displayMsg " Received move "
                    displaySeparator
                    displayGameState gCfg respGameHistory
                    displaySeparator

                    return respGameHistory



interpretatingMsgFromResponse :: HttpResponse -> IO [TicTacToeMsg]
interpretatingMsgFromResponse resp = do

    let msg = getHttpMessageContentsOnly resp

    case interpretateMsgFromStr msg of

        Left err          -> fail err
        Right gameHistory -> return gameHistory



-- playGameGet :: HttpRequest -> GameCfg -> IO [TicTacToeMsg]
-- playGameGet req gCfg = do
--     -- displayMsg "-------- GET -------------"
--     -- print req
--     resp <- httpGet req
--     case resp of
--       Left err      -> fail $ show err
--       Right respMsg -> do
--           let msg = getHttpMessageContentsOnly respMsg
--
    --           case interpretateMsgFromStr msg of
--             Left err          -> fail err
--             Right gameHistory -> do
--
    --                 displaySeparator
--                 displayMsg " Received move "
--                 displaySeparator
--                 displayGameState gCfg gameHistory
--                 displaySeparator
--
    --                 return gameHistory
