{-# LANGUAGE OverloadedStrings, GeneralizedNewtypeDeriving #-}

module Server.TicTacToe (main, mainDefault) where

import Control.Applicative
import Control.Concurrent.STM
import Control.Monad.Reader

import Data.Default.Class
import Data.String
import Data.Text.Lazy (Text)

import Network.Wai.Middleware.RequestLogger

import Prelude

import Web.Scotty.Trans



--Custom imports:

import qualified Data.Set as Set
import Data.List

import Data.String.Conversions

import GameInfo
import ActionsWinner
import ActionsMove
import Parse.Bencode
import Parse.TicTacToeMsg
import Parse.MsgPlusMap


defaultPort = 3000


mainDefault :: IO ()
mainDefault = main defaultPort


main :: Int -> IO ()
main port = do
    sync <- newTVarIO def
        -- 'runActionToIO' is called once per action.
    let runActionToIO m = runReaderT (runWebM m) sync

    scottyT port runActionToIO app



-- Game session info {{{1

data GameSession = GameSession
    { gameSessionId      :: String
    , gameSessionHistory :: [TicTacToeMsg]
    } deriving(Show)

instance Eq GameSession where
    s1 == s2 = str1 == str2
        where
            str1 = gameSessionId s1
            str2 = gameSessionId s2

instance Ord GameSession where
    compare s1 s2 = compare str1 str2
        where
            str1 = gameSessionId s1
            str2 = gameSessionId s2

--}}}1



data AppState = AppState
    { appStateGameSessionsHistory :: Set.Set GameSession}

instance Default AppState where
    def = AppState Set.empty

--Utils from Scotty transformer {{{1

-- Also note: your monad must be an instance of 'MonadIO' for
-- Scotty to use it.
newtype WebM a = WebM { runWebM :: ReaderT (TVar AppState) IO a }
    deriving (Applicative, Functor, Monad, MonadIO, MonadReader (TVar AppState))

-- Scotty's monads are layered on top of our custom monad.
-- We define this synonym for lift in order to be explicit
-- about when we are operating at the 'WebM' layer.
webM :: MonadTrans t => WebM a -> t WebM a
webM = lift


-- Some helpers to make this feel more like a state monad.
gets :: (AppState -> b) -> WebM b
gets f = f <$> (ask >>= liftIO . readTVarIO)
modify :: (AppState -> AppState) -> WebM ()
modify f = ask >>= liftIO . atomically . flip modifyTVar' f

-- }}}1



toHtmlStringFromGameSessionList :: [GameSession] -> String
toHtmlStringFromGameSessionList games =
    "<ul>" ++ toHtmlStringFromGameSessionList' games

toHtmlStringFromGameSessionList' :: [GameSession] -> String
toHtmlStringFromGameSessionList' [] = "</ul>"
toHtmlStringFromGameSessionList' (game:xs) =
    ("<li>"++ gameSessionId game++ "</li>")
    ++ toHtmlStringFromGameSessionList' xs

-- UNUSED
toHtmlStringFromGameMoves :: GameSession -> String
toHtmlStringFromGameMoves game =
    concat
        ["<h1> Game ID: "
        , gameSessionId game
        ,"</h1>"
        , "<h2>Last game move dump</h2>"
        ,"<p>"
        , toHtmlStringFromGameMoves' game 1
        , "</p>"]

-- UNUSED
toHtmlStringFromGameMoves' :: GameSession -> Integer -> String
toHtmlStringFromGameMoves' game _ = show game
-- generateMoveHtml' game number
--    | (number `mod` 2 == 1) = "<p>----------------</p>" ++ generateMoveHtml' game (number+1)
--    | otherwise =

findGameSession :: [GameSession] -> String -> Maybe GameSession
findGameSession ss sessionId = find ((== sessionId) . gameSessionId) ss





displaySeparator :: IO ()
displaySeparator = displayMsg $ replicate 80 '-'

displayGameState :: [TicTacToeMsg] -> IO ()
displayGameState = print

displayMsg :: String -> IO ()
displayMsg = putStrLn



constructGameInfo :: [TicTacToeMsg] -> ActionT Text WebM GameInfo
constructGameInfo gameHistory =
    case toGameInfo False gameHistory of

        Left err       -> error err
        Right gameInfo -> return gameInfo


constructNewGameHistory :: String -> ActionT Text WebM [TicTacToeMsg]
constructNewGameHistory msg =
    case interpretateMsgFromStr msg of
        Left err          -> fail err
        Right gameHistory -> do

            -- displaySeparator
            -- displayMsg " Received move "
            -- displaySeparator
            -- displayGameState gameHistory
            -- displaySeparator

            case moveBasic False gameHistory of

                Left err            -> return gameHistory
                Right (val, coords) -> do

                    let tttMsg =
                            TicTacToeMsg
                                { msgId         = UserId "2"
                                , msgValue      = val
                                , msgCoordinate = coords
                                }

                        newHistory     = updateTicTacToeMsg gameHistory tttMsg

                    return newHistory


updateGameSessionsHistory :: GameSession -> AppState -> AppState
updateGameSessionsHistory s appS =
    AppState{ appStateGameSessionsHistory = newSessionHistory }
    where newSessionHistory = Set.insert s $ appStateGameSessionsHistory appS





-- This app doesn't use raise/rescue, so the exception
-- type is ambiguous. We can fix it by putting a type
-- annotation just about anywhere. In this case, we'll
-- just do it on the entire app.
app :: ScottyT Text WebM ()
app = do
    middleware logStdoutDev
    get "/" $ redirect "/history"
        -- c <- webM $ gets tickCount
        -- text $ fromString $ show c

    get "/history" $ do
        historySet <- webM $ gets appStateGameSessionsHistory
        let history = Set.toList historySet

        html $ mconcat [cs (toHtmlStringFromGameSessionList history)]

    get "/history/:gameId" $ do
        sessionId  <- param "gameId" :: ActionT Text WebM Text
        historySet <- webM $ gets appStateGameSessionsHistory
        let history = Set.toList historySet
            gameSessionFromHistory = findGameSession history (cs sessionId)

        case gameSessionFromHistory of
          Just gs -> html ( cs (toHtmlStringFromGameMoves gs))
          Nothing -> html "<h1> Game not found </h1> "


    post "/game/:gameId/player/1" $ do

        gameId  <- param "gameId" :: ActionT Text WebM Text
        reqBody <- body

        let gameIdStr  = cs gameId  :: String
            reqBodyStr = cs reqBody :: String

        gameHistory <- constructNewGameHistory reqBodyStr
        -- gameHistory <- gameHistoryIO

        let updatedGameSession =
                GameSession
                    { gameSessionId = gameIdStr
                    , gameSessionHistory = gameHistory
                    }

        webM $ modify $ updateGameSessionsHistory updatedGameSession

        gameInfo <- constructGameInfo gameHistory
        -- when (winnerCheck gameInfo || fullBoard gameInfo)
        --     $ do
        --         displaySeparator
        --         displayMsg "Game has finished."
        --         displaySeparator

        let encodedHistory = composeBencode $ obscureMsg gameHistory

        raw $ cs encodedHistory

