module Main where

import System.Environment

import Debug.Trace

import Data.Maybe
import Data.Either
import Data.List

import Control.Monad

import Utils
import qualified Server.TicTacToe as Sttt
import qualified Client.TicTacToe as Cttt


defaultHost = "localhost"
defaultPort = 3000


flagServer = "s"
flagClient = "c"


helpMainText s =

       "\n"
    ++ s
    ++ " : "
    ++ flagServer
    ++" / "
    ++ flagClient
    ++ "\n"
    ++ "Options:"
    ++ "\n"
    ++ helpServerText
    ++ "\n"
    ++ helpClientText
    ++ "\n"

helpServerText = " * " ++ flagServer ++ " [<port>]"
helpClientText = " * " ++ flagClient ++ " <gameId> [<host>] [<port>]"



main :: IO ()
main = do
    args <- getArgs
    pgName <- getProgName

    case args !!? 0 of

        Just flag -> do
            displaySeparator
            if flag == flagServer
               then callServer
               else callClient
            displaySeparator

        _ -> putStrLn $ helpMainText pgName


callServer = do
    args   <- getArgs
    displayMsg "Initializing Server"
    let p = fromMaybe defaultPort $ readMaybe  =<< (args !!? 1)

    Sttt.main p


callClient = do
    args   <- getArgs
    pgName <- getProgName
    displayMsg "Initializing Client"
    case args !!? 1 of

        Nothing -> putStrLn $ helpMainText pgName
        Just gId -> do
            let h = fromMaybe defaultHost (args !!? 2)
                p     = fromMaybe defaultPort $ readMaybe  =<< (args !!? 3)

            Cttt.main h p gId



displaySeparator :: IO ()
displaySeparator = displayMsg $ replicate 80 '-'

displayMsg :: String -> IO ()
displayMsg = putStrLn
