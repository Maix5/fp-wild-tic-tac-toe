module ParserSpec where

import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck
import Control.Exception (evaluate)
import Data.Either
import Data.Maybe

import Parse.Bencode
import Parse.MsgPlusMap
import Parse.TicTacToeMsg
import Test.Parse.Bencode

import ActionsMove
import ActionsWinner
import GameInfo


bencodeStringNothing = "d1:cd1:0i0e1:1i2ee2:id0:4:prevd1:cd1:0i2e1:1i2ee2:id0:1:v1:oe1:v1:xe"

-- Winner "VgyaQL"
bencodeStringWinnerName = "VgyaQL"
bencodeStringWinner = "d1:cd1:0i2e1:1i0ee2:id6:VgyaQL4:prevd1:cd1:0i1e1:1i2ee2:id24:MXSwRFqwMNzFPfvnMfyPtsGE4:prevd1:cd1:0i2e1:1i2ee2:id6:VgyaQL4:prevd1:cd1:0i1e1:1i0ee2:id24:MXSwRFqwMNzFPfvnMfyPtsGE4:prevd1:cd1:0i0e1:1i0ee2:id6:VgyaQL4:prevd1:cd1:0i0e1:1i1ee2:id24:MXSwRFqwMNzFPfvnMfyPtsGE4:prevd1:cd1:0i1e1:1i1ee2:id6:VgyaQL1:v1:oe1:v1:oe1:v1:oe1:v1:oe1:v1:xe1:v1:oe1:v1:xe"

-- Collision on (0, 1)
bencodeStringCollision = "d1:cd1:0i0e1:1i2ee2:id16:qPaRQfjLsrCtckoE4:prevd1:cd1:0i1e1:1i2ee2:id22:HpdhmkRqbEFKKUyKuBThBX4:prevd1:cd1:0i1e1:1i1ee2:id16:qPaRQfjLsrCtckoE4:prevd1:cd1:0i0e1:1i1ee2:id22:HpdhmkRqbEFKKUyKuBThBX4:prevd1:cd1:0i2e1:1i1ee2:id16:qPaRQfjLsrCtckoE4:prevd1:cd1:0i1e1:1i0ee2:id22:HpdhmkRqbEFKKUyKuBThBX4:prevd1:cd1:0i0e1:1i1ee2:id16:qPaRQfjLsrCtckoE1:v1:oe1:v1:oe1:v1:oe1:v1:oe1:v1:xe1:v1:oe1:v1:xe"

bencodeStringEmpty = "de"

-- Name "nKsmulGKQ" collision
bencodeStringNameCollision = "d1:cd1:0i2e1:1i0ee2:id9:nKsmulGKQ4:prevd1:cd1:0i0e1:1i1ee2:id9:nKsmulGKQ1:v1:oe1:v1:xe"




main :: IO ()
main = hspec $ do
    describe "Bencode parsing" $ do
        parseBencodeFailSpec "bencodeStringNothing"
            bencodeStringNothing True
        parseBencodeFailSpec "bencodeStringWinner"
            bencodeStringWinner True
        parseBencodeFailSpec "bencodeStringCollision"
            bencodeStringCollision True
        parseBencodeFailSpec "bencodeStringEmpty"
            bencodeStringEmpty True
        parseBencodeFailSpec "bencodeStringNameCollision"
            bencodeStringNameCollision True

    describe "Bencode composing" $ do
        composeBencodeFailSpec "bencodeStringNothing"
            bencodeStringNothing True
        composeBencodeFailSpec "bencodeStringWinner"
            bencodeStringWinner True
        composeBencodeFailSpec "bencodeStringCollision"
            bencodeStringCollision True
        composeBencodeFailSpec "bencodeStringEmpty"
            bencodeStringEmpty True
        composeBencodeFailSpec "bencodeStringNameCollision"
            bencodeStringNameCollision True


        modifyMaxSize (const 500)
            $ it "Parses and encodes data info with string format."
            $ property prop_bencodeDataComposingInversible




    describe "Bencode interpretating" $ do
        interpBencodeFailSpec "bencodeStringNothing"
            bencodeStringNothing True
        interpBencodeFailSpec "bencodeStringWinner"
            bencodeStringWinner True
        interpBencodeFailSpec "bencodeStringCollision"
            bencodeStringCollision True
        interpBencodeFailSpec "bencodeStringEmpty"
            bencodeStringEmpty True
        interpBencodeFailSpec "bencodeStringNameCollision"
            bencodeStringNameCollision True

    describe "Bencode obscuratingting" $ do
        obscurateInterpBencodeFailSpec "bencodeStringNothing"
            bencodeStringNothing True
        obscurateInterpBencodeFailSpec "bencodeStringWinner"
            bencodeStringWinner True
        obscurateInterpBencodeFailSpec "bencodeStringCollision"
            bencodeStringCollision True
        obscurateInterpBencodeFailSpec "bencodeStringEmpty"
            bencodeStringEmpty True
        obscurateInterpBencodeFailSpec "bencodeStringNameCollision"
            bencodeStringNameCollision True


    describe "Winner detection" $ do
        winnerDetectSpec "bencodeStringNothing"
            bencodeStringNothing False
        winnerDetectSpec "bencodeStringWinner"
            bencodeStringWinner True
        winnerDetectSpec "bencodeStringCollision"
            bencodeStringCollision False
        winnerDetectSpec "bencodeStringEmpty"
            bencodeStringEmpty False
        winnerDetectSpec "bencodeStringNameCollision"
            bencodeStringNameCollision False

        winnerCheckNameSpec "bencodeStringWinner"
            bencodeStringWinner bencodeStringWinnerName True



    -- describe "absolute" $ do
    --     it "returns the original number when given a positive input" $
    --         1 `shouldBe` 1
    --
        --     it "returns a positive number when given a negative input" $
    --         (1) `shouldBe` 1
    --
        -- describe "Prelude.head" $ do
    --     it "returns the first element of a list" $ do
    --         head [23 ..] `shouldBe` (23 :: Int)
    --
        --     it "returns the first element of an *arbitrary* list" $
    --         property $ \x xs -> head (x:xs) == (x :: Int)
    --
        --     it "throws an exception if used with an empty list" $ do
    --         evaluate (head []) `shouldThrow` anyException


parseBencodeFailSpec desc s res =
    it ("Parse.Bencode (parseBencode) " ++ desc) $
        isRight (parseBencode s) `shouldBe` res

interpBencodeFailSpec desc s res =
    it ("Parse.Bencode (parseBencode) " ++ desc) $
        isRight (interpretateMsg $ parseBencode s) `shouldBe` res


composeBencodeFailSpec desc s res =
    it ("Parse.Bencode (composerBencode) " ++ desc) $
        prop_composeBencode s `shouldBe` res

obscurateInterpBencodeFailSpec desc s res =
    it ("Parse.Bencode (parseBencode) " ++ desc) $
        prop_bencodeInterp s `shouldBe` res


winnerDetectSpec desc s res =
    it ("ActionsWinner (winnerCheck) " ++ desc) $
        case toGameInfo True =<< interpretateMsg (parseBencode s) of
          Left _ -> False
          Right gI -> winnerCheck gI
        `shouldBe` res

winnerCheckNameSpec desc s n res =
    it ("ActionsWinner (winnerCheckName) " ++ desc) $
        case toGameInfo True =<< interpretateMsg (parseBencode s) of
          Left _ -> False
          Right gI ->
              case winnerCheckName gI of
                Nothing -> False
                Just name -> maybe False (== UserId n) name
        `shouldBe` res


prop_composeBencode x =
    case parseBencode x of
      Left _ -> False
      Right val -> composeBencode val == x

prop_bencodeInterp x =
    case interpretateMsg $ parseBencode x of
      Left _ -> False
      Right val -> composeBencode (obscureMsg val) == x
